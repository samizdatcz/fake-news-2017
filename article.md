title: "„Nad Evropou se valí radioaktivní mrak.“ Přinášíme žebříček falešných zpráv pro rok 2017"
perex: "<small> Letošní vymyšlené zprávy mají i desetitisíce sdílení - a svým autorům přinášejí nezanedbatelný zisk. </small>"
authors: ["Michal Zlatkovský"]
published: "23. května 2017"
coverimg: https://upload.wikimedia.org/wikipedia/commons/1/1e/Fake_News_%2832332513102%29.jpg
coverimg_note: "Foto <a href='#'>Wikimedia Commons</a>"
libraries: ["https://unpkg.com/jquery@3.2.1", "//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js","https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"]
styles: ["//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css","https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css"] 
options: "" #wide
---

Zatímco Polsko vystupuje z Evropské unie, česká armáda přechází pod německé velení a nad Evropou se valí radioaktivní mrak, Donald Trump odstraňuje z Bílého domu islámské náboženské symboly. Tak letos vidí svět čtenáři webů, které se specializují na šíření <i>fake news</i> - překroucených nebo vymyšlených zpráv.

O jejich nebezpečnosti se začalo celosvětově mluvit loni v období amerických prezidentských voleb. Před půlrokem jsme proto [provedli analýzu](https://interaktivni.rozhlas.cz/dezinformace/) českých textů z dezinformačních webů, která ukázala, že šíření falešných zpráv zdaleka není okrajovým jevem ani v Česku. A z dnešního přehledu textů za prvních pět měsíců roku 2017 plyne, že jejich vliv je na českém internetu stále silný.

Za dezinformační a manipulativní weby označila několik desítek stránek [studie think-tanku Evropské hodnoty](http://www.evropskehodnoty.cz/fungovani-ceskych-dezinformacnich-webu/weby_list/), z níž kromě serveru iROZHLAS.cz vycházejí i [odborníci](https://www.irozhlas.cz/zpravy-domov/vsechny-fake-news-nelze-odhalit-maji-fakticky-zaklad-a-pusobi-verohodne-varuje-odbornik-_201612020555_ogolis), kteří fenomén <i>fake news</i> studují. 

Dezinformační weby fungují nejčastěji na principu míchání skutečného zpravodajství, v jádru pravdivých, ale senzačně zpracovaných informací a částěčně či úplně nepravdivých textů. Jejich nejúspěšnější texty byly loni na sociálních sítích [sdílenější](https://interaktivni.rozhlas.cz/dezinformace/) než ty z tradičních médií.

I když se v roce 2017 se ve velkém sdílejí články z větších i menších stránek, dlouhodobě nejčtenějším dezinformačním webem zůstavají Parlamentní listy. „Z mého pohledu jsou nejnebezpečnější,“ řekl serveru iROZHLAS.cz politolog Miloš Gregor, spoluautor analýzy zaměřené na dezinformace. 

„Právě proto, že řada obsahu je fakticky správně, takže vzbuzují důvěru, ale poměrně často se tam objeví zpravodajsky neseriózní a velice emotivně zabarvené výkřiky. To už zavání tendencí čtenáři manipulovat. Najdeme tam i konspirační teorie i hoaxy.“

Přehled jsme sestavili za použití stejné metody [jako loni](https://interaktivni.rozhlas.cz/dezinformace/). V první tabulce je desítka nejsdílenějších zpráv, které lze jednoznačně označit za nepravdivé, s faktickými nesmysly v titulku nebo textu zprávy.

<wide>
<table id="filtered" class="display" width="100%"></table>
<div style="margin-left: 20px"><i>Data: <a href="https://app.buzzsumo.com">Buzzsumo</a>. Aktuální k 22. květnu 2017.</i></div>
</wide>

Stejně jako loni se dezinformační weby drží témat imigrace a islámu. Je to případ prvních dvou článků v žebříčku. První z nich tvrdí, že Polsko hrozí odchodem z EU, pokud bude nuceno příjmout uprchlíky podle kvót schválených Evropskou unií. Ačkoliv článek vychází ze skutečné kritiky postoje EU k uprchlíkům ze strany polské premiérky Beaty Szydlové, výhružka odchodem z unie nikdy nepadla.

Druhý text o údajných vztazích německých lékařek a muslimských imigrantů má přes 20 000 sdílení. Na webu svobodnenoviny.eu vyšel v lednu, jde však o překlad anglického textu z anonymního blogu z konce roku 2015, tedy doby, kdy migrační krize vrcholila. 

Druhá tabulka uvádí nepravdivé zprávy, označené červeně, v kontextu všech textů sdílených z dezinformačních webů. Mezi ty patří komentář bývalého místopředsedy Bloku proti islámu Petra Hampla o EET, exkluzivní zpráva o milosti pro Jiřího Kajínka, ale také rozhovor s 94letým generálem Emilem Bočkem. Jde o typický produkt Parlamentních listů. U těch stále platí, že se [většinou](https://zpravy.aktualne.cz/domaci/parlamentni-listy/r~b0558610268b11e7afda0025900fea04/?redirected=1495115850) vyhýbají zjevně vymyšleným zprávám. Neprokazatelné informace a skandální výroky ale často nechávají pronášet respondenty v rozhovorech. 

<wide>
<table id="unfiltered" class="display" width="100%"></table>
</wide>

Z desítky nejsdílenějších článků z dezinformačních webů lze za falešné zprávy přímo označit polovinu. Kromě dvou zmíněných zpráv jsou to oba články z proruského konspiračního webu Aeronet a zpráva o tom, že americký prezident Donald Trump nechal z Bílého domu odstranit islámské náboženské symboly. Původně přitom šlo o zprávu vymyšlenou [protitrumpovským táborem](https://www.buzzfeed.com/craigsilverman/a-hoax-about-trump-removing-islamic-symbols-from-the-white-h?utm_term=.ywr4QzQm4#.orX36m6z3).

## Příjmy dezinformátorů? Hlavně z reklamy

Jednou z motivací autorů dezinformačních webů může být finanční zisk. Výdělky jednotlivých stránek se přitom různí, jak vyplývá z analýzy, kterou pro iROZHLAS.cz zpracovalo sdružení [Czech Publisher Exchange](https://www.cpex.cz/). „Výdělek většiny dezinformačních webů bude podle dostupných údajů o návštěvnosti velmi nízký, kolem 10 až 20 tisíc korun, často i méně,“ říká ředitel sdružení Matěj Novák.

Výjimkou je web AC24, u kterého analýza odhadla měsíční příjem z reklamy kolem 50 tisíc, a Parlamentní listy, které si na reklamě vydělají půl miliónu až milión měsíčně. Ty však získávají i peníze z jiných zdrojů - například [prostřednictvím PR článků placených veřejnou správou](https://www.irozhlas.cz/zpravy-domov/kraj-platil-za-oslavne-clanky-o-haskovi-urad-utratil-statisice-na-parlamentnich-listech_201608041255_pholinkova). „Některé weby, jako například Sputnik News nebo Aeronet, reklamu vůbec neprodávají,“ dodává Novák. „Je tedy pravděpodobné, že jsou financovány jinak.“

Ty weby, které na reklamě vydělávají, používají reklamní systémy Seznam Sklik a Google AdSense. Seznam zároveň některé z nich, například inStory.cz, propaguje na své titulní stránce v sekci se „zajímavými články z českého internetu“. Jak ale říká mluvčí Seznamu Irena Zatlouková, problematiku dezinformačních webů server právě analyzuje. „Můžu prozradit, že kromě právních otázek teď řešíme například i to, zda existují strojové metriky na odhalení [falešných zpráv]. Určitě se tedy tomuto tématu věnujeme a jakmile skončíme fázi analýz, přejdeme k činům.“